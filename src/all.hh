#ifndef ALL_HH_
# define ALL_HH_

# include <mln/core/image2d.hh>
# include <mln/value/rgb8.hh>

using mln::image2d;
using mln::point2d;
using mln::value::rgb8;

/// Load a color image from \a path.
image2d<rgb8> load (const char* path);
/// Save a gray-level stretched image \a img to \a path.
void save (const image2d<float>& img, const char* path);

/// Compute the energy in \a img using the norm of the gradient.
image2d<float> get_nrg (const image2d<rgb8>& img);
/** Adjust an existing energy computation.  Optimized way of adjusting the
 *  energy in an image from which a seam has been carved.
 *  \param img The input image.
 *  \param nrg The existing energy computation of \a img.
 *  \param path The vertical seam which has been removed from \a img and
 *  from which the energy must be re-computed.
 */
void adjust_vertical_nrg (const image2d<rgb8> img, image2d<float> nrg,
                          std::vector<unsigned> path);
/// Compute the entropy in \a img using a 9x9 window.
image2d<float> get_entropy (const image2d<rgb8>& img);

/** Find a vertical seam in the image.  The process only relies on the
 *  energy in the image.
 *  \param nrg The energy in the image.
 *  \return A vector with the points in the seam.  Let \c c be the \c r th
 *  value in the vector.  This means that the seam passes by the point
 *  (r, c) in the image.
 */
std::vector<unsigned>
find_vertical_seam (const image2d<float> nrg);

/** Carve a seam out of an image.
 *  \param img Image to modify.
 *  \param path Path of the seam to carve.
 *  \return \a img with the seam \a path carved out.
 */
template <typename T>
image2d<T>
carve_vertical_seam (const image2d<T> img, const std::vector<unsigned>& path);

image2d<rgb8>
uncarve_vertical_seam (const image2d<rgb8> img, const std::vector<unsigned>& path);

#endif /* !ALL_HH_ */
