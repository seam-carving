#include <mln/border/duplicate.hh>
#include <mln/core/dpoint2d.hh>
#include <mln/norm/l2.hh>

#include "all.hh"

using namespace mln;

static inline
float
get_local_nrg (const image2d<rgb8>& i, const point2d& p)
{
  using norm::l2_distance;
  typedef metal::vec<3u, float> vec_t;
  vec_t l = i (p + left).to_equiv ();
  vec_t r = i (p + right).to_equiv ();
  vec_t u = i (p + up).to_equiv ();
  vec_t d = i (p + down).to_equiv ();
  return l2_distance (l, r) + l2_distance (u, d);
}

static inline
float
get_local_nrg (const image2d<rgb8> i, unsigned r, unsigned c)
{
  return get_local_nrg (i, point2d (r, c));
}

image2d<float> get_nrg (const image2d<rgb8>& img)
{
  image2d<float> nrg (img.domain ());
  image2d<rgb8>::fwd_piter p (img.domain ());
  border::duplicate (img);

  for_all (p)
    nrg (p) = get_local_nrg (img, p);
  return nrg;
}

void adjust_vertical_nrg (const image2d<rgb8> img, image2d<float> nrg,
                          std::vector<unsigned> path)
{
  assert (img.domain () == nrg.domain ());
  unsigned rows = geom::nrows (img);

  for (unsigned r = 0; r < rows; ++r)
  {
    unsigned seam_at_col = path.at (r);
    if (seam_at_col > 0)
      nrg.at (r, seam_at_col - 1) = get_local_nrg (img, r, seam_at_col - 1);
    nrg.at (r, seam_at_col) = get_local_nrg (img, r, seam_at_col);
  }
}
