#include <cmath>
#include <limits>
#include <vector>

#include <mln/border/duplicate.hh>
#include <mln/geom/ncols.hh>
#include <mln/geom/nrows.hh>
#include <mln/literal/colors.hh>

#include "all.hh"

using namespace mln;

/// Generic 3-argument implementation of \c min.
template <typename T>
static inline
const T&
min (const T& v1, const T& v2, const T& v3)
{
  return std::min (v1, std::min (v2, v3));
}

/** \internal
 *  \param nrg The energy in a given image.
 *  \return an image that contains the cumulative minimum energy for all
 *  possible connected seam in \a nrg.  Implements the function \c M
 *  described in the paper of Avidan and Shamir, section 3.
 */
static inline
image2d<float>
accumulate_nrg (const image2d<float> nrg)
{
  image2d<float> accu (nrg.domain ());
  border::duplicate (nrg);
  unsigned cols = geom::ncols (nrg);
  unsigned rows = geom::nrows (nrg);

  // Initialize the first line with the nrg
  for (unsigned c = 0; c < cols; ++c)
    accu.at (0, c) = nrg.at (0, c);

  for (unsigned r = 1; r < rows; ++r)
    for (unsigned c = 0; c < cols; ++c)
      accu.at (r, c) = nrg.at (r, c)
        + min (accu.at (r - 1, c != 0 ? c - 1 : c),
               accu.at (r - 1, c),
               accu.at (r - 1, c != cols - 1 ? c + 1 : c));
  return accu;
}

/** \internal
 *  \return the minimum value on the \a row of \a accu_nrg, by considering
 *  only the values in the range [ \a colmin .. \a colmax [.
 */
static inline
unsigned
get_min_nrg_on_row (const image2d<float> accu_nrg, unsigned row,
                    unsigned colmin, unsigned colmax)
{
  assert (colmin < colmax);
  float nrg = std::numeric_limits<float>::max ();
  unsigned c = colmin;
  unsigned mincol = colmin;

  for (; c < colmax; ++c)
    if (accu_nrg.at (row, c) < nrg)
    {
      nrg = accu_nrg.at (row, c);
      mincol = c;
    }
  return mincol;
}

/** \internal
 * \return the minimum value on the \a row of \a accu_nrg.
 */
static inline
unsigned
get_min_nrg_on_row (const image2d<float> accu_nrg, unsigned row)
{
  return get_min_nrg_on_row (accu_nrg, row, 0, geom::ncols (accu_nrg));
}

/** \internal
 *  Find the path of the seam in an image containing the cumulative minimum
 *  energy.
 *  \param accu_nrg The cumulative minimum energy for all possible connected
 *  seam (called \c M in the paper of Avidan and Shamir, section 3).
 */
static inline
std::vector<unsigned>
find_path (const image2d<float> accu_nrg)
{
  unsigned rows = geom::nrows (accu_nrg);
  unsigned cols = geom::ncols (accu_nrg);
  assert (rows > 1);
  std::vector<unsigned> seam_path (rows);

  // Find the minimum on the last line
  seam_path.at (rows - 1) = get_min_nrg_on_row (accu_nrg, rows - 1);

  // Find the path by following the minimums backwards
  unsigned r = rows - 1;
  do {
    assert (r > 0);
    unsigned cur_col = seam_path.at (r);
    --r;
    seam_path.at (r) = get_min_nrg_on_row (accu_nrg, r,
                                           cur_col ? cur_col - 1 : 0U,
                                           std::min (cur_col + 2, cols));
  } while (r);

  return seam_path;
}

std::vector<unsigned>
find_vertical_seam (const image2d<float> nrg)
{
  image2d<float> accu_nrg = accumulate_nrg (nrg);
  std::vector<unsigned> seam_path = find_path (accu_nrg);
  return seam_path;
}

template <typename T>
image2d<T>
carve_vertical_seam (const image2d<T> img, const std::vector<unsigned>& path)
{
  unsigned cols = geom::ncols (img);
  unsigned rows = geom::nrows (img);
  image2d<T> carved (rows, cols - 1);

  for (unsigned r = 0; r < rows; ++r)
  {
    unsigned seam_at_col = path.at (r);
    for (unsigned c = 0; c < seam_at_col; ++c)
      carved.at (r, c) = img.at (r, c);
    for (unsigned c = seam_at_col; c < cols; ++c)
      carved.at (r, c) = img.at (r, c + 1);
  }

  return carved;
}

// Instantiate for the types we use.
template image2d<rgb8>
carve_vertical_seam<rgb8>(const image2d<rgb8> img,
                          const std::vector<unsigned>& path);
template image2d<float>
carve_vertical_seam<float>(const image2d<float> img,
                           const std::vector<unsigned>& path);

image2d<rgb8>
uncarve_vertical_seam (const image2d<rgb8> img, const std::vector<unsigned>& path)
{
  unsigned cols = geom::ncols (img);
  unsigned rows = geom::nrows (img);
  image2d<rgb8> uncarved (rows, cols + 1);

  for (unsigned r = 0; r < rows; ++r)
  {
    unsigned seam_at_col = path.at (r);
    for (unsigned c = 0; c < seam_at_col; ++c)
      uncarved.at (r, c) = img.at (r, c);
    uncarved.at (r, seam_at_col) = mln::literal::red;
    for (unsigned c = seam_at_col; c < cols; ++c)
      uncarved.at (r, c + 1) = img.at (r, c);
  }

  return uncarved;
}
