#include <iostream>

#include <mln/border/duplicate.hh>
#include <mln/border/resize.hh>
#include <mln/io/ppm/load.hh>
#include <mln/io/pgm/save.hh>
#include <mln/level/stretch.hh>

using namespace mln;
using namespace mln::value;

image2d<rgb8> load (const char* path)
{
  image2d<rgb8> img = io::ppm::load<rgb8> (path);
  border::thickness = 4;
  border::resize (img, 4);
  border::duplicate (img);
  return img;
}

void save (const image2d<float>& img, const char* path)
{
  std::cerr << "saving" << std::endl;
  image2d<int_u8> gl (img.domain ());
  level::stretch (img, gl);
  io::pgm::save (gl, path);
  std::cerr << "end" << std::endl;
}
