#include <iostream>

#include "all.hh"

using namespace mln;
using namespace mln::value;

int main (int argc, char** argv)
{
  if (argc != 3)
  {
    std::cerr << "usage: " << *argv << " in.ppm out.pgm\n";
    return 1;
  }

  image2d<rgb8> img = load (argv[1]);
  image2d<float> nrg = get_nrg (img);
  save (nrg, argv[2]);
}
