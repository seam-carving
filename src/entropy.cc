#include <cmath>

#include <ext/hash_map>
namespace std
{
  using __gnu_cxx::hash_map;
}

#include <mln/win/rectangle2d.hh>

#include "all.hh"

using namespace mln;

float
log_without_nan (float v)
{
  assert (v > 0);
  float l = log (v);
  if (std::isnan (l))
    throw "log => nan";
  return l;
}

namespace __gnu_cxx
{
  template<>
  struct hash<rgb8>
  {
    std::size_t operator() (const rgb8& col) const
    {
      return (col.red () << (sizeof (col.red ()) + sizeof (col.green ())))
        + (col.green () << sizeof (col.green ()))
        + col.blue ();
    }
  };
}

static inline
float
get_local_entropy (const image2d<rgb8>& img, const point2d& p)
{
  win::rectangle2d rect (9, 9);
  win::rectangle2d::fwd_qiter q (rect, p);

  std::hash_map<rgb8, float> probas;
  // Computer the number of occurrence of each pixel.
  for_all (q)
    ++probas[img (q)];

  // Transform in a probability \in [0; 1]
  for (std::hash_map<rgb8, float>::iterator i = probas.begin ();
       i != probas.end ();
       ++i)
  {
    i->second /= 9 * 9;
    assert (i->second >= 0);
    assert (i->second <= 1);
  }

  // Compute the entropy of `p'.
  rgb8 pt = img (p);
  float entropy = probas[pt] * log_without_nan (probas[pt]);
  if (std::isnan (entropy))
    return 0; // I'm not sure about this.
  return -entropy;
}

image2d<float> get_entropy (const image2d<rgb8>& img)
{
  image2d<float> entropy (img.domain ());
  image2d<rgb8>::fwd_piter p (img.domain ());

  for_all (p)
    entropy (p) = get_local_entropy (img, p);
  return entropy;
}
