#include <iostream>
#include <stack>
#include <vector>

#include <boost/lexical_cast.hpp>

#include <mln/border/duplicate.hh>
#include <mln/io/ppm/save.hh>
#include <mln/level/fill.hh>

#include "all.hh"

using namespace mln;
using namespace mln::value;

static inline
void
add_bias (image2d<float> nrg, const char* path)
{
  image2d<rgb8> tmp = load (path);
  assert (tmp.domain () == nrg.domain ());
  image2d<rgb8>::fwd_piter p (tmp.domain ());

  for_all (p)
    nrg (p) += tmp (p).red () - tmp (p).green ();
}

/// \file Find a vertical seam in an image.

int main (int argc, char** argv)
{
  if (argc != 5 && argc != 6)
  {
    std::cerr << "usage: " << *argv
              << " N in.ppm carved.ppm seams.ppm [bias.ppm]\n"
      "where N is an integer > 0: the number of seams to carve.\n";
    return 1;
  }

  unsigned nseam = 1;

  try {
    nseam = boost::lexical_cast<unsigned> (argv[1]);
  }
  catch (const boost::bad_lexical_cast&)
  {
    std::cerr << "invalid 1st argument" << std::endl;
    return 2;
  }

  std::cerr << "initializing ... ";
  image2d<rgb8> img = load (argv[2]);
  border::duplicate (img);
  image2d<float> nrg = get_nrg (img);
  if (argc >= 6)
    add_bias (nrg, argv[5]);
  std::stack<std::vector<unsigned> > seams;

  for (unsigned i = 0; i < nseam; ++i)
  {
    std::cerr << (i + 1) << "..";
    seams.push (find_vertical_seam (nrg));
    img = carve_vertical_seam (img, seams.top ());
    nrg = carve_vertical_seam (nrg, seams.top ());
    adjust_vertical_nrg (img, nrg, seams.top ());
    border::duplicate (img);
    image2d<float> new_nrg = get_nrg (img);
  }
  std::cerr << '.';
  io::ppm::save (img, argv[3]);
  for (unsigned i = 0; i < nseam; ++i)
  {
    img = uncarve_vertical_seam (img, seams.top ());
    seams.pop ();
  }
  std::cerr << '.';
  io::ppm::save (img, argv[4]);
  std::cerr << std::endl;
}
