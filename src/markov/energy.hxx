# include "energy.hh"

# include <mln/fun/v2v/norm.hh>
# include <mln/norm/all.hh>

# include <mln/accu/all.hh>
# include <mln/level/all.hh>
# include <mln/level/take.spe.hh>

# include <mln/morpho/laplacian.hh>
# include <mln/morpho/gradient.hh>
# include <mln/border/resize.hh>

# include <mln/win/rectangle2d.hh>
# include <mln/core/neighb2d.hh>
# include <mln/geom/ncols.hh>

# include <map>
# include <set>

template <typename value_type>
template <typename point_type>
Energy<value_type>::Energy(const mln::image2d<point_type>& ima, unsigned nb)
  : ima_(ima.domain()), seams_(ima.domain()), seams_nb_(nb)
{
  using mln::image2d;

  // convert to level of gray
  mln::fun::v2v::l1_norm<typename point_type::equiv, value_type> norm;
  image2d<value_type> gray_ima(ima.domain());
  typename image2d<point_type>::fwd_piter p(ima.domain());

  e_max_ = 0;
  for_all(p)
  {
    const value_type n = mln::norm::l1(ima(p).to_equiv());
    gray_ima(p) = n;
  }
  // for_all(p)
  //   gray_ima(p) = gray_ima(p) / e_max_;

  // Computes absolute value of gradient
  mln::border::resize(gray_ima, 1);
  ima_ = mln::morpho::gradient(gray_ima, mln::win::rectangle2d(3, 3));
  mln::level::abs_inplace(ima_);

  mln::accu::max_<value_type> max;
  mln::level::take(ima_, max);
  e_max_ = max.to_result();
}

template <typename value_type>
value_type
Energy<value_type>::data_energy(mln::point2d p) const
{
  value_type e;

  if (seams_(p))
    e = ima_(p) / e_max_;
  else
    e = (e_max_ - ima_(p)) / e_max_;

  // Count the number of true point on seams_(p.rows(),*), if
  // non-equal to 'n' (the desired number of seams) then the energy
  // should be greater than otherwise.
  const unsigned row = p.to_point().row();
  const unsigned ncols = mln::geom::ncols(seams_);
  unsigned cnt = 0;
  for (unsigned col = 0; col < ncols; ++col)
    if (seams_.at(row, col))
      ++cnt;
  if (cnt == seams_nb_ && seams_(p))
    return e;
  else
    return abs(cnt - seams_nb_) * e;
}

template <typename value_type>
value_type
Energy<value_type>::context_energy(mln::point2d p) const
{
  using mln::point2d;
  using mln::dpoint2d;

  double e = 0;

  // Only authorize 4-clique of two white points.
  std::set<point2d> pset;
  std::map<point2d, bool> val;
  pset.insert(pset.begin(), p + dpoint2d(-1, 0));
  pset.insert(pset.begin(), p + dpoint2d(1, 0));
  pset.insert(pset.begin(), p + dpoint2d(0, 1));
  pset.insert(pset.begin(), p);


  for (std::set<point2d>::iterator pp = pset.begin();
       pp != pset.end(); ++pp)
    if (seams_(*pp))
      val[*pp] = true;

  if (val.size() > 2 || (val.size() == 2 && val[p] == false))
    e += 10;

  pset.clear();
  val.clear();

  pset.insert(pset.begin(), p + dpoint2d(-1, -1));
  pset.insert(pset.begin(), p + dpoint2d(1, -1));
  pset.insert(pset.begin(), p + dpoint2d(0, -1));
  pset.insert(pset.begin(), p);

  for (std::set<point2d>::iterator pp = pset.begin();
       pp != pset.end(); ++pp)
    if (seams_(*pp))
      val[*pp] = true;

  if (val.size() > 2 || (val.size() == 2 && val[p] == false))
    e += 10;
  return ((e < 20) ? e : 10);
}

template <typename value_type>
value_type
Energy<value_type>::energy(mln::point2d p) const
{
  return data_energy(p) + context_energy(p);
}

static inline
const mln::neighb2d& c10()
{
  static bool flower = true;
  static mln::neighb2d it;
  if (flower)
  {
    // c8.
    it.insert(mln::make::dpoint2d(0, 1));
    it.insert(mln::make::dpoint2d(1,-1));
    it.insert(mln::make::dpoint2d(1, 0));
    it.insert(mln::make::dpoint2d(1, 1));
    // last 1.
    it.insert(mln::make::dpoint2d(2, 0));
  }
  return it;
}

