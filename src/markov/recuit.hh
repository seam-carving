#ifndef RECUIT_HH
# define RECUIT_HH

# include "energy.hh"

template <typename value_type>
void recuit(Energy<value_type>& energy);

# include "recuit.hxx"
#endif // RECUIT_HH
