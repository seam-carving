#ifndef ENERGY_HH_
# define ENERGY_HH_

# include <mln/core/image2d.hh>
# include <mln/core/neighb2d.hh>

static
const mln::neighb2d& c10();

template <typename value_type>
class Energy
{
public:
  typedef mln::image2d<value_type> energy_map;
  typedef typename energy_map::fwd_piter fwd_piter;

  template <typename point_type>
  Energy(const mln::image2d<point_type>&, unsigned nb);

  value_type energy(mln::point2d) const;

protected:

  Energy() {};
  value_type data_energy(mln::point2d) const;
  value_type context_energy(mln::point2d) const;

public:
  energy_map ima_;
  mln::image2d<bool> seams_;
  value_type e_max_;
  unsigned seams_nb_;
};

#include "energy.hxx"

#endif // ENERGY_HH_
