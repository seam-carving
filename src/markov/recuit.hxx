#ifndef RECUIT_HXX
# define RECUIT_HXX

# include <cstdlib>
# include "energy.hh"

template <typename value_type>
void recuit(Energy<value_type>& E)
{
  double T = 1000;
  bool change = true;
  unsigned row_inc = 3;
  unsigned col_inc = 4;
  unsigned min_change = E.ima_.ncells() / 16;
  const unsigned ncols = mln::geom::ncols(E.seams_);
  const unsigned nrows = mln::geom::nrows(E.seams_);
  typedef typename Energy<value_type>::fwd_piter fwd_piter;

  while (T > 0.001 || change)
  {
    unsigned cnt = 0;
    unsigned change_cnt = 0;
    value_type delta_mean = 0;

    for (unsigned row_shift = 0; row_shift < row_inc; ++row_shift)
      for (unsigned col_shift = 0; col_shift < col_inc; ++col_shift)
	for (unsigned i = row_shift; i < nrows; i += row_inc)
	  for (unsigned j = col_shift; j < ncols; j += col_inc)
	  {
	    const mln::point2d p(i, j);
	    //std::cerr << "at point : " << p <<std::endl;;

	    value_type eold = E.energy(p);
	    E.seams_(p) = !E.seams_(p);
	    value_type enew = E.energy(p);
	    value_type delta_u = eold - enew;

	    double r = random();
	    // Refus probabiliste de la modification
	    if (delta_u >= 0)
	      if ((r / RAND_MAX) > std::exp(-delta_u / T))
		E.seams_(p) = !E.seams_(p);
	      else
	      {
		delta_mean += delta_u;
		change_cnt++;
	      }
	    else
	    {
	      delta_mean += delta_u;
	      change_cnt++;
	    }
	    ++cnt;
	  }

    change = (change_cnt >= min_change);
    T *= 0.95;
    std::cerr << "mean delta_u: " << delta_mean / change_cnt << std::endl;
    std::cerr << change_cnt << " changes occured" << std::endl;
    std::cerr << "temp: " << T << std::endl;
  }
}


#endif // RECUIT_HXX
