# include <iostream>
# include <mln/core/image2d.hh>
# include <mln/fun/v2v/norm.hh>
# include <mln/value/all.hh>
# include <mln/border/all.hh>
# include <mln/value/float01_f.hh>
# include <mln/value/graylevel.hh>

# include <mln/morpho/gradient.hh>
# include <mln/morpho/laplacian.hh>
# include <mln/win/all.hh>
# include <mln/metal/vec.hh>

# include <mln/io/ppm/all.hh>
# include <mln/io/pgm/save.hh>
# include <mln/io/pbm/save.hh>
# include <mln/display/all.hh>

# include <cstdlib>
# include <cmath>

# include "energy.hh"
# include "recuit.hh"

int main(int argc, char **argv)
{
  assert(argc == 3);
  std::string filename(argv[1]);

  srandom(time(0));
  using namespace mln;
  image2d<value::rgb8> ima;

  io::ppm::load(ima, filename);
  Energy<double> E(ima, atoi(argv[2]));

  image2d<double>::fwd_piter p(ima.domain());
  for_all(p)
    E.seams_(p) = rand() % 2;

  mln::accu::max_<double> max;
  mln::level::take(E.ima_, max);
  double emax = max.to_result();
  image2d<value::int_u8> out(ima.domain());
  for_all(p)
    out(p) = ((unsigned)round(E.ima_(p) / emax * 255));

  io::pgm::save(out, filename + ".energy" + ".pgm");

  recuit(E);

  io::pbm::save(E.seams_, filename + ".seams" + ".pbm");

  return 0;
}
